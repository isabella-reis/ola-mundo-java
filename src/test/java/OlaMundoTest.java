import org.junit.Test;
import org.junit.Assert;

public class OlaMundoTest {

    @Test
    public void deveDizerOlaQuandoNomeForInformado() {
        OlaMundo olaMundo = new OlaMundo();

        String resposta = olaMundo.dizerOla(  "Isabella");

        Assert.assertEquals(  "Olá Isabella :)", resposta);
    }
}
